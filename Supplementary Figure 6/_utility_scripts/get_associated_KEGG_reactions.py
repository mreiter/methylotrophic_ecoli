####
# Find associated KEGG reactions to a given Breseq output (list of mutations)
####
import pickle
import requests
import pandas as pd
import requests
import re

def add_entry_to_key(d, at, g):
    if at not in d.keys():
        d[at] = [g]
    else:
        d[at].append(g)
    return d

def get_kegg_gene_id(response_text, gene):
    organism_list = ['eco:', 'ecj:', 'ecd:', 'ebw:', 'ecok:', 'ece:', 'ecs:', 'ecf:', 'etw:', 'elx:', 'eoi:', 'eoj:', 'eoh:', 'ecoo:', 'ecoh:', 'esl:', 'eso:', 'esm:', 'eck:', 'ecg:', 'ecoi:' ]# list of e. coli species identifiers
    results = re.split('\t|\n', response_text)
    eco_entry = []
    for i, result in enumerate(results):
        if any(x in result for x in organism_list) and (gene in results[i+1]):
            # this filter allows annotations from all E. coli species, not only MG1655
            return [result, results[i+1]]


def get_associated_kegg_reactions_api(genes):
    # expect list of genes that were mutated (make sure to not have duplicates in that list)
    # return pandas dataframe with the following columns: 'gene', 'gene id', 'orthology ID', 'reaction ID', 'reaction'
    ########
    # find reactions for list of genes
    ########
    # querying many E. coli databases to miss as few reactins as possible
    # above analysis only done on E. coli MG1655 (eco) annotation database (does not contain all annotations that are relevant)



    # find all associated reactions for a list of genes
    no_entries = []     # list of all genes for which there are no entries in the KEGG DB
    no_ko = []          # list of all genes for which there is no KEGG orthology ID and presumably also no reaction
    no_reaction = {}    # dict of all genes for which there are no associated reaction entries in the KEGG DB
    reactions = {}

    skip_list = [] # list of genes to skip (search results are weird and are irrelevant to analysis)
    for gene in genes:
        # get kegg gene ID for gene
        gene_request = requests.get('http://rest.kegg.jp/find/genes/eco+{}'.format(gene))
        if gene_request.text == '\n':
            print('couldn\'t find entry for {}'.format(gene))
            no_entries.append(gene)
            continue
        else:
            kegg_gene_id = get_kegg_gene_id(gene_request.text, gene)

            # get kegg orthology ID for gene
            print(gene, kegg_gene_id)
            ko_request = requests.get('http://rest.kegg.jp/link/ko/{}'.format(kegg_gene_id[0]))
            if (ko_request.text == '\n') or (ko_request.text == None):
                print('no KO entry for {} in KEGG DB'.format(gene))
                no_ko.append(gene)
                continue
            else:
                ko = re.split('\t|\n', ko_request.text)[1]
                ko = re.split(':', ko)[1]

            # get kegg reaction ID
            rnID_request = requests.get('http://rest.kegg.jp/link/reaction/{}'.format(ko))
            if rnID_request.text == '\n':
                print('no reaction data for {} in KEGG DB'.format(gene))
                no_reaction[gene] = [kegg_gene_id, ko]
            else:
                # reactions dictionary entries:
                reactions[gene] = {'gene': gene, 'gene ID': kegg_gene_id[0], 'orthology ID': ko, 'reaction ID': '', 'reaction': ''}

                reactionIDs = [x for x in re.split('\t|\n', rnID_request.text) if (x != '') and ('ko' not in x)]
                for i, id in enumerate(reactionIDs):
                    id = re.split(':', id)[1]
                    reaction_request = requests.get('http://rest.kegg.jp/get/{}'.format(id))
                    reaction_entry = re.split('\n', reaction_request.text)
                    reaction = re.split('  ', reaction_entry[2])[1]

                    if i == 0:
                        reactions[gene]['reaction ID'] = id
                        reactions[gene]['reaction'] = reaction
                    else:
                        gene_i = gene + str(i)
                        reactions[gene_i] = reactions[gene].copy()
                        reactions[gene_i]['reaction ID'] = id
                        reactions[gene_i]['reaction'] = reaction
    return pd.DataFrame(list(reactions.values())), no_entries, no_ko, no_reaction
