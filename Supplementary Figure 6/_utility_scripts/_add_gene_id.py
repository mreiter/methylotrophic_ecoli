# add gene KEGG gene ID column to 'MR_proteomics.xlsx'

import pandas as pd
import os

os.listdir('../../../../_databases/KEGG_downloads/')


df = pd.read_excel('./data/MR_proteomics.xlsx', skiprows=11)

df['gene'] = df['Accession'].str.split('_', expand=True).iloc[:, 4]


df_kegg_gene_IDs = pd.read_csv('../../../../_databases/KEGG_downloads/Ecoli_genes/kegg_eco_genes.txt', sep='\t', header=None)
df_kegg_gene_IDs[0] = df_kegg_gene_IDs[0].str.split(':', expand=True).iloc[:,1]
df_kegg_gene_IDs[1] = df_kegg_gene_IDs[1].str.split('; ', expand=True).iloc[:,0]
df_kegg_gene_IDs.columns = ['KEGG_gene_ID', 'gene']


df_ID = pd.merge(df, df_kegg_gene_IDs, on='gene')
df_ID.to_excel('./data/MR_proteomics_edited.xlsx', index=False)
