# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 14:11:56 2021

@author: kellerp
"""

import copy
import cobra
from cobra.io import read_sbml_model
import numpy as np
import pandas as pd

def calculate_fluxes(model, knockouts_set, carbon_sources_set, target_growth_rate):
    df = pd.DataFrame()
    for knockouts in knockouts_set:
        df_strain = calculate_fluxes_for_strain(model, knockouts, carbon_sources_set, target_growth_rate)
        df = df.append(df_strain)
    return df

def calculate_fluxes_for_strain(model, knockouts, carbon_sources, target_growth_rate):
    ko_model = copy.deepcopy(model)
    insert_knockouts(ko_model, knockouts)
    results = calculate_fluxes_for_conditions(ko_model, knockouts, carbon_sources_set, target_growth_rate)
    return results

def insert_knockouts(model, knockouts):
    knockouts = knockouts.split("|")
    for knockout in knockouts:
        model.reactions.get_by_id(knockout).knock_out()
    return model

def calculate_fluxes_for_conditions(model, knockouts, carbon_sources_set, target_growth_rate):
    df = pd.DataFrame()
    for carbon_sources in carbon_sources_set:
        df_fluxes = calculate_flux_for_condition(model, knockouts, carbon_sources, target_growth_rate)
        df = pd.concat([df, df_fluxes], axis = 1)
    return df

def calculate_minimal_methanol_influx_for_target_growth_rate(model, carbon_sources, target_growth_rate):
    condition_model = copy.deepcopy(model)
    for carbon_source in carbon_sources.split("|"):
        if carbon_source == "methanol":
            condition_model.reactions.get_by_id("EX_" + carbon_source + "_e").bounds = -1, 0
        else:
            condition_model.reactions.get_by_id("EX_" + carbon_source + "_e").bounds = -10, 0
    result = condition_model.optimize()
    slope = result.objective_value/1.0
    return target_growth_rate/slope
    

def calculate_flux_for_condition(model, knockouts, carbon_sources, target_growth_rate):
    condition_model = copy.deepcopy(model)
    
    minimal_methanol_influx = calculate_minimal_methanol_influx_for_target_growth_rate(model, carbon_sources, target_growth_rate)
    
    for carbon_source in carbon_sources.split("|"):
        if carbon_source == "methanol":
            print(minimal_methanol_influx)
            condition_model.reactions.get_by_id("EX_" + carbon_source + "_e").bounds = (-1)*minimal_methanol_influx, 0
        else:
            condition_model.reactions.get_by_id("EX_" + carbon_source + "_e").bounds = -10, 0
        
    result = cobra.flux_analysis.flux_variability_analysis(condition_model, condition_model.reactions, fraction_of_optimum=1, loopless=True, processes=1)
    
    result.where(result.apply(np.absolute) > float("1e-10"), 0, inplace = True)
    
    result["flux center"] = result[["minimum", "maximum"]].mean(axis=1)
    result["flux span"] = np.absolute(result["maximum"] - result["minimum"])
    result["flux span percentage"] = result["flux span"]/np.absolute(result["flux center"])*100
    
    result = result.add_suffix("_" + str(carbon_sources))
    
    return result

def count_reactions_with_large_relative_flux_span(df):
    result = open("result.txt", "w")
    result.write("total number of reactions model: " + str(len(df.index)) + "\n")
    result.write("\n" + "number of reactions with a flux span greater than 5% of the flux center:" + "\n")
    condition_list = []
    for column in df.columns:
        if "percentage" in column:
            condition = column.split("_")[1]
            if condition not in condition_list:
                reaction_count = 0
                condition_list.append(condition)
                for reaction in df.index:
                    if (df.loc[reaction][column] > 5.0) & (df.loc[reaction][column] > 0.001):
                        reaction_count = reaction_count + 1
                result.write(condition + ": " + str(reaction_count) + "\n")
    result.close()

def calculate_average_RuMP_cycle_flux(df):
    result = open("result.txt", "a")
    result.write("\naverage rump cycle fluxes:\n")
    rump_reactions = ["TALA","TKT1","TKT2","RPE","RPI","H6PS","H6PI","MEDH"]
    condition_list = []
    rump_fluxes = []
    for column in df.columns:
        if "flux center" in column:
            condition = column.split("_")[1]
            if condition not in condition_list:
                for reaction in df. index:
                    if reaction in rump_reactions:
                        rump_fluxes.append(df.loc[reaction][column])
                condition_list.append(condition)
                average_rump_cycle_flux = np.average(rump_fluxes)
                result.write(condition + ": " + str(average_rump_cycle_flux) + "\n")
    result.close()
        
model = read_sbml_model("core_model_with_rump_frmA_fdh.xml")

model.reactions.get_by_id("ME1").bounds = -1000,1000
# model.reactions.get_by_id("ME2").bounds = 0,0

target_growth_rate = 0.2
                        
knockouts_set = ["TPI|FRMA|FDH"]

carbon_sources_set = ["methanol|pyr","methanol"]

df = calculate_fluxes(model, knockouts_set, carbon_sources_set, target_growth_rate)

df.sort_index(inplace = True)

df.to_excel("results.xlsx")

count_reactions_with_large_relative_flux_span(df)
calculate_average_RuMP_cycle_flux(df)