import pandas as pd
def add_entry_to_key(d, at, g):
    if at not in d.keys():
        d[at] = [g]
    else:
        d[at].append(g)
    return d

def load_breseq(path_to_output):
    # load Breseq output .html file
    results = pd.read_html(path_to_output, skiprows=1)

    mutations_simple = results[0].copy()

    # dataframe cleanup
    header = [x[0] for x in mutations_simple.columns]
    entry0 = [x[1] for x in mutations_simple.columns]

    mutations_simple.columns = header
    mutations_simple.loc[-1] = entry0
    mutations_simple.index = mutations_simple.index + 1

    mutations_simple.sort_index(inplace=True)

    mutations_simple['freq'] = mutations_simple['freq'].str.strip('%').astype(float) / 100

    mutations_simple['annotation'] = mutations_simple['annotation'].astype(str)
    mutations_simple['annotation'] = mutations_simple['annotation'].str.split().str.join(' ')       # to get rid of unicode characters e.g. \xa0 for space
    mutations_simple['gene'] = mutations_simple['gene'].str.split().str.join(' ')
    return mutations_simple

def filter_breseq_coding(mutations_df, freq_cutoff, non_synonymous_flag=True):
    # frequency cutoff
    mutations_simple_fco = mutations_df.copy()[mutations_df['freq'] >= freq_cutoff]

    # filter for coding mutations
    mask_non_coding = ~mutations_simple_fco['annotation'].str.contains('intergenic|pseudogene|noncoding')       #noncoding mutations are in sRNA
    # mask_pseudogene = ~mutations_simple_fco['annotation'].str.contains('pseudogene')
    mutations_simple_fco_c = mutations_simple_fco.copy()[mask_non_coding]

    if (mutations_simple_fco_c.shape[0] > 0) and (non_synonymous_flag):
        # treat small indels in coding sequences as non-synonymous mutations
        indel_mask = mutations_simple_fco_c['mutation'].str.contains('bp|\+|\-')

        mutations_simple_fco_c_indel = mutations_simple_fco_c[indel_mask].copy()
        mutations_simple_fco_c_pm = mutations_simple_fco_c[~indel_mask].copy()


        if mutations_simple_fco_c_pm.shape[0] > 0:
            # non-synonymous aa mutations
            mutations_simple_fco_c_pm['annotation'] = mutations_simple_fco_c_pm['annotation'].str.split(expand=True).iloc[:,0]
            mutations_simple_fco_c_pm[['WT_aa', 'pos_aa', 'mut_aa']] = mutations_simple_fco_c_pm['annotation'].str.split('([0-9]+)', expand=True)
            mutations_simple_fco_c_pm['pos_aa'] = mutations_simple_fco_c_pm['pos_aa'].astype(int)
            mutations_simple_fco_c_pm['gene'] = mutations_simple_fco_c_pm['gene'].str.split(expand=True).iloc[:,0]
            mask_ns = mutations_simple_fco_c_pm['annotation'].str[0] != mutations_simple_fco_c_pm['annotation'].str[-1]
            mutations_simple_fco_c_pm_ns = mutations_simple_fco_c_pm.copy()[mask_ns]

        else:
            mutations_simple_fco_c_pm_ns = mutations_simple_fco_c_pm.copy()
            mutations_simple_fco_c_pm_ns['WT_aa'] = 'NaN'
            mutations_simple_fco_c_pm_ns['pos_aa'] = 'NaN'
            mutations_simple_fco_c_pm_ns['mut_aa'] = 'NaN'

        if mutations_simple_fco_c_indel.shape[0] > 0:
            # indels
            mutations_simple_fco_c_indel['annotation'] = mutations_simple_fco_c_indel['annotation'].str.split(expand=True).iloc[:,0]
            mutations_simple_fco_c_indel['WT_aa'] = 'NaN'
            mutations_simple_fco_c_indel['pos_aa'] = 'NaN'
            mutations_simple_fco_c_indel['mut_aa'] = 'NaN'
            mutations_simple_fco_c_indel['gene'] = mutations_simple_fco_c_indel['gene'].str[:-2]
        else:
            mutations_simple_fco_c_indel['WT_aa'] = 'NaN'
            mutations_simple_fco_c_indel['pos_aa'] = 'NaN'
            mutations_simple_fco_c_indel['mut_aa'] = 'NaN'

            # return concatenated df
        return pd.concat([mutations_simple_fco_c_pm_ns, mutations_simple_fco_c_indel])

    elif (mutations_simple_fco_c.shape[0] > 0) and (not non_synonymous_flag):
        # return all mutations in coding regions
        mutations_simple_fco_c['annotation'] = mutations_simple_fco_c['annotation'].str.split(expand=True).iloc[:,0]
        mutations_simple_fco_c['gene'] = mutations_simple_fco_c['gene'].str.split(expand=True).iloc[:,0]
        return mutations_simple_fco_c
    else:
        print('no coding mutations')
        return mutations_simple_fco_c
