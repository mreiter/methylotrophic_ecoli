# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 09:58:47 2021

@author: kellerp
"""

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams['font.family'] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

#load dataset from textfile (copy paste from excel sheet)
data = pd.read_excel("Chemostat evolution Overview.xlsx")

data["Time [d]"] = data["Time [h]"]/24

#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}


#--plot--------------------------------------------------------------------------------------------------------------------
plt.figure(figsize=(4,2.5))

fig = sns.lineplot(x='Time [d]', y='OD600',
                   data=data,
                   marker="o",
                   markersize=8,
                   markeredgewidth=0.5,
                   markeredgecolor="None",
                   lw=2,
                   ci="sd",
                   err_style='bars',
                   err_kws = err_kws,
                   legend = False
                   )

fig.set(xlim=(0, 438),ylim=(0,2.6))

fig.set_xlabel(xlabel = "time [d]")
fig.set_ylabel(ylabel = "$\mathregular{OD_{600}}$")

plt.gca().get_lines()[0].set_color("black")

#save the figure
fig.figure.savefig("Time [d] vs OD600" + ".pdf", bbox_inches="tight")