import pandas as pd
import plotly.graph_objects as go

#load dataset from excel file
data_in = pd.read_csv('./growth data G249.txt', sep='\t')
data_in


# plot data
data = []
t = data_in.columns[1:].astype(float)
t

data.append(go.Scatter(x = t, y = data_in.iloc[1, 1:],  line={'width':2.5, 'color': '#9B9A9A'}, marker={'size': 10}))
data.append(go.Scatter(x = t, y = data_in.iloc[0, 1:], line={'width': 2.5, 'color': "#2D2E83"}, marker={'size': 10}))



layout = go.Layout(yaxis={'title': 'OD<sub>600</sub>', 'range': [-0.05, 1.5], 'showgrid':False, 'linecolor': 'black', 'mirror': True},
                   xaxis={'title': 'time [h]', 'showgrid':False, 'linecolor': 'black', 'mirror': True},

                   font_family='Arial',
                   font_size=14,
                   font_color='black',
                   paper_bgcolor='rgba(0,0,0,0)',
                   plot_bgcolor='rgba(0,0,0,0)',
                   showlegend=False,
                   width=350,
                   height=225,
                   margin={'l':50, 'r':0, 't':0, 'b':0}
                    )
fig = go.Figure(data=data, layout=layout)
fig.show()
fig.write_image('./Figure 3a G249.pdf')

# %%


#transpose data as a first step to later fit to seaborn format
data = data.T
#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}
#change first row to header row and set all values to float format
header = data.iloc[0]
data = data[1:]
data = data.astype(float)
data = data.rename(columns = header)
#reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
data = data.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")

#--plot--------------------------------------------------------------------------------------------------------------------
plt.figure(figsize=(4,2.5))

palette = [("#2D2E83"),
           ("#2D2E83"),
           ("#2D2E83"),
           ("#9B9A9A"),
           ("#9B9A9A"),
           ("#9B9A9A")]

fig = sns.lineplot(x='Time [h]', y='$\mathregular{OD_{600}}$', hue='Conditions',
                   data=data,
                   palette=palette,
                   marker="o",
                   markersize=8,
                   markeredgewidth=0.5,
                   markeredgecolor="None",
                   lw=2,
                   ci="sd",
                   err_style='bars',
                   err_kws = err_kws,
                   legend = False
                   )

fig.set(xlim=(0, None),ylim=(0.,2.0))

fig.set_xlabel(xlabel = "time [h]")

#save the figure
fig.figure.savefig("Figure 3b MEcoli_ref_1" + ".pdf", bbox_inches="tight")
