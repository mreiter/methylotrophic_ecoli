import pandas as pd
import plotly.graph_objects as go

#load dataset from excel file
data_in = pd.read_csv('./growth data g534.csv', sep='\t')



# plot data
data = []
t = data_in.columns[1:].astype(float)

print(data_in)
for i in range(data_in.shape[0]):
    print(i)
    data.append(go.Scatter(x = t, y = data_in.iloc[i, 1:],  line={'width':2.5, 'color': '#2D2E83'}, marker={'size': 10}))

# data.append(go.Scatter(x = t, y = data_in.iloc[0, 1:], line={'width': 2.5, 'color': "#2D2E83"}, marker={'size': 10}))



layout = go.Layout(yaxis={'title': 'OD<sub>600</sub>', 'range': [-0.05, 3.0], 'showgrid':False, 'linecolor': 'black', 'mirror': True},
                   xaxis={'title': 'time [h]', 'showgrid':False, 'linecolor': 'black', 'mirror': True},

                   font_family='Arial',
                   font_size=14,
                   font_color='black',
                   paper_bgcolor='rgba(0,0,0,0)',
                   plot_bgcolor='rgba(0,0,0,0)',
                   showlegend=False,
                   width=350,
                   height=225,
                   margin={'l':50, 'r':0, 't':0, 'b':0}
                    )
fig = go.Figure(data=data, layout=layout)
fig.show()
fig.write_image('./Supplementary Figure 1.pdf')

# %%
