# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:37:56 2020

@author: kellerp
"""

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator)

plt.rcParams["font.family"] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

data = pd.read_excel("isotopologue_distribution_12C_CO2.xlsx")

def plot_bar(data_isotopologue, bottom):
    width = 0.8
    color_list = ["#2D2E83","#57589C","#8182B5","#ABABCD","#CFCFD7","#5F3E40","#876E70","#171742"]
    plt.bar(data_isotopologue["metabolite"],
        data_isotopologue["isotopologue fraction"],
        width,
        bottom=bottom,
        color=color_list[data_isotopologue["isotopologue"].values[0]],
        yerr=data_isotopologue["std"],
        capsize=3.0
        )
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)

def plot_isotopologue_distribution(data, metabolite_list):    
    
    plt.subplots(figsize=(2.75,2.5))
    
    for metabolite in metabolite_list:
        subdata = data[data["metabolite"] == metabolite]
        isotopologue_list=[]
        for isotopologue in subdata["isotopologue"]:
            data_isotopologue = subdata[subdata["isotopologue"] == isotopologue]
            bottom = 0
            for iso in isotopologue_list:
                bottom = bottom + subdata[subdata["isotopologue"] == iso].iloc[0]["isotopologue fraction"]
            isotopologue_list.append(isotopologue)
            plot_bar(data_isotopologue, bottom)
    # Add some text for labels, title and custom x-axis tick labels, etc.
    plt.ylabel('isotopologue fraction')
    plt.xticks(metabolite_list, rotation = "vertical",ha="center")
    plt.ylim(0.0, 1.1)
    plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0])
    plt.tight_layout()
    plt.savefig("Supplementary Figure 2a isotopologue_distribution_12C_CO2.pdf")
    plt.savefig("Supplementary Figure 2a isotopologue_distribution_12C_CO2.png", dpi=600)
    plt.show()
    

metabolite_list = ["H6P","P5P","S7P","2PG/3PG","Arg","Asn","Gln","Lys"]

plot_isotopologue_distribution(data, metabolite_list)

