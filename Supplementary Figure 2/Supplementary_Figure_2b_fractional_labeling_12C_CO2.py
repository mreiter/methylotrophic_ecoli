# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:37:56 2020

@author: kellerp
"""

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams["font.family"] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

data = pd.read_excel("fractional_labeling_12C_CO2.xlsx")

print(data)

data_replicates = pd.read_excel("fractional_labeling_replicates_12C_CO2.xlsx")

def plot_fractional_labeling(data, data_replicates, metabolite_list):
    data_measured = data
    
    x = np.arange(len(metabolite_list))  # the label locations
    width = 0.8  # the width of the bars
    
    print(data_measured)
    
    fig, ax = plt.subplots(figsize=(2.75,2.5))
    ax.bar(x, data_measured["average"],
           width,
           yerr=data_measured["std"],
           capsize=3.0,
           color=("#6F6F6E"),
           error_kw=dict(capthick=1)
           )
    
    j=0
    for metabolite in metabolite_list:
        for replicate in data_replicates[data_replicates["metabolite"] == metabolite].iterrows():
            ax.scatter(x[j],
                       y = replicate[1]["value"],
                       s=10,
                       facecolors="none",
                       edgecolors="black",
                       linewidths=1.0,
                           zorder=4)
        j = j+1
    
    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('labeled fraction')
    ax.set_xticks(x)
    ax.set_xticklabels(metabolite_list, rotation = "vertical",ha="center")
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.legend(loc="upper left",frameon=False)
    plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0])
    plt.ylim(0.0, 1.1)
    plt.tight_layout()
    plt.savefig("Supplementary Figure 2b fractional_labeling_12C_CO2.pdf")
    plt.savefig("Supplementary Figure 2b fractional_labeling_12C_CO2.png", dpi=600)
    
    
metabolite_list = ["H6P","P5P","S7P","2PG/3PG","Arg","Asn","Gln","Lys"]

plot_fractional_labeling(data, data_replicates, metabolite_list)
