# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 14:11:56 2021

@author: kellerp
"""

import copy
from cobra.io import read_sbml_model
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams["font.family"] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def insert_knockouts(model, knockouts):
    knockouts = knockouts.split("|")
    for knockout in knockouts:
        model.reactions.get_by_id(knockout).knock_out()
    return model
        
def create_summary_dataframe(flux, result, reaction_type, model, scaling_number):
    df = pd.DataFrame({"reaction abbreviation": [flux],
                       "reaction name": [model.reactions.get_by_id(flux).name],
                       "reaction": [model.reactions.get_by_id(flux)],
                       "reaction type": [reaction_type],
                       "reaction rate": [abs(result.fluxes[flux])*scaling_number]})
    return df

model = read_sbml_model("core_model_with_rump_frmA_fdh.xml")
                    
knockouts_set = "TPI|FRMA|FDH"

substrate = "methanol"

ko_model = copy.deepcopy(model)
ko_model = insert_knockouts(ko_model, knockouts_set)

ko_model.reactions.get_by_id("EX_" + substrate + "_e").bounds = -100,0
ko_model.reactions.get_by_id("ME1").bounds = -1000,1000

result = ko_model.optimize()

scaling_number = 0.0856 /result.objective_value

total_carboxylation_flux = 0
total_decarboxylation_flux = 0

result_df = pd.DataFrame()

for flux in result.fluxes.index:
    if "co2_c" in ko_model.reactions.get_by_id(flux).reaction:
        co2_substrate = str(ko_model.reactions.get_by_id(flux)).split(">")[0]
        co2_product = str(ko_model.reactions.get_by_id(flux)).split(">")[1]
        if "CO2t" not in flux:
            if "co2_c" in co2_substrate:
                if result.fluxes[flux] > 0:
                    total_carboxylation_flux = total_carboxylation_flux + abs(result.fluxes[flux])
                    result_df = result_df.append(create_summary_dataframe(flux, result, "carboxylation", model, scaling_number))
                else:
                    total_decarboxylation_flux = total_decarboxylation_flux + abs(result.fluxes[flux])
                    result_df = result_df.append(create_summary_dataframe(flux, result, "decarboxylation", model, scaling_number))
            
            elif "co2_c" in co2_product:
                if result.fluxes[flux] < 0:
                    total_carboxylation_flux = total_carboxylation_flux + abs(result.fluxes[flux])
                    result_df = result_df.append(create_summary_dataframe(flux, result, "carboxylation", model, scaling_number))
                else:
                    total_decarboxylation_flux = total_decarboxylation_flux + abs(result.fluxes[flux])
                    result_df = result_df.append(create_summary_dataframe(flux, result, "decarboxylation", model, scaling_number))
                    
substrate_input = abs(ko_model.reactions.get_by_id("EX_" + substrate + "_e").flux)*scaling_number
total_carboxylation_flux = total_carboxylation_flux*scaling_number
total_decarboxylation_flux = total_decarboxylation_flux*scaling_number
fraction_of_total_carbon_assimilation_substrate_input = substrate_input/(substrate_input + total_carboxylation_flux)
fraction_of_total_carbon_assimilation_carboxylation = total_carboxylation_flux/(substrate_input + total_carboxylation_flux)

print("substrate: "+ substrate)
print("substrate input: " + str(substrate_input))
print("fraction of total carbon assimilation: " + str(fraction_of_total_carbon_assimilation_substrate_input))
print("total CO2 fixed: " + str(total_carboxylation_flux))
print("fraction of total carbon assimilation: " + str(fraction_of_total_carbon_assimilation_carboxylation))
print("total CO2 released: " + str(total_decarboxylation_flux))
print("growth rate: " + str((ko_model.reactions.get_by_id("BIOMASS_Ecoli_core_w_GAM").flux)*scaling_number))

def plot_bar(x_label, flux, bottom, color):
    width = 0.8
    plt.bar(x_label,
        flux,
        width,
        bottom=bottom,
        color=color,
        
        )
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)

plt.subplots(figsize=(3,4.5))
   
total_carbon_incorporated = substrate_input + total_carboxylation_flux - total_decarboxylation_flux
  
bottom = 0
plot_bar("carbon assimilation", substrate_input, bottom, "#8DA586")
bottom = bottom + substrate_input
plot_bar("carbon assimilation", total_carboxylation_flux, bottom, "#BBC9B6")
bottom = 0
plot_bar("carbon dissimilation", total_decarboxylation_flux, bottom, "#978979")

plt.ylabel('reaction rate [mmol gCDW-1 h-1]')
plt.xticks(rotation = "45",ha="center")
plt.yticks([0,1.0,2.0,3.0,4.0,5.0])
plt.tight_layout()
plt.savefig("Supplementary Figure 3.pdf")
plt.show()

result_df.to_excel("result.xlsx")
print(result_df)
                

    

